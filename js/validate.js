function showMessage(idTag, message) {
  document.getElementById(idTag).innerHTML = message;
}
function kiemTraTrung(id, dsnv) {
  let viTri = dsnv.findIndex(function (nv) {
    return nv._tknv == id;
  });
  if (viTri != -1) {
    showMessage("tbTKNV", "Tài khoản đã tồn tại");
    return false;
  } else {
    return true;
  }
}
