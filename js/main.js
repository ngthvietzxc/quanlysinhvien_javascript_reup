// Function Thêm nhân viên
var dsnv = [];
const DSNV_LOCAL = "DSNV_LOCAL";
// Khi user load trang => lấy dữ liệu từ localStorage
var jsonData = localStorage.getItem(DSNV_LOCAL);
if (jsonData != null) {
  dsnv = JSON.parse(jsonData);
  renderDSNV(dsnv);
}
console.log("jsonData: ", jsonData);
function themNV() {
  // Lấy thông tin từ form
  var nv = layThongTinTuForm();
  // Thêm phần tử vào array
  dsnv.push(nv);
  // Convert data
  let dataJson = JSON.stringify(dsnv);
  // Lưu vào localStorage
  localStorage.setItem(DSNV_LOCAL, dataJson);
  // Render danh sách nhân viên
  renderDSNV(dsnv);
  resetForm();
}
// Function Xóa Nhân viên
function xoaNV(id) {
  console.log("Xóa id: ", id);
  // splice
  var viTri = -1;
  for (var i = 0; i < dsnv.length; i++) {
    if (dsnv[i]._tknv == id) {
      viTri = i;
    }
  }
  if (viTri != -1) {
    // nếu tìm thấy vị trí thì xóa
    // splice(vị trí, số lượng)
    dsnv.splice(viTri, 1);
    renderDSNV(dsnv);
  }
}
// Function Sửa Nhân viên
function suaNV(id) {
  console.log("id", id);
  var viTri = dsnv.findIndex(function (item) {
    return item._tknv == id;
  });
  console.log("suaNV -> vitri", viTri);
  // Show thông tin lên form
  var nv = dsnv[viTri];
  document.getElementById("tknv").value = nv._tknv;
  document.getElementById("name").value = nv._name;
  document.getElementById("email").value = nv._email;
  document.getElementById("password").value = nv._password;
  document.getElementById("datepicker").value = nv._datepicker;
  document.getElementById("luongCB").value = nv._luongCB;
  document.getElementById("chucVu").value = nv._chucVu;
  document.getElementById("gioLam").value = nv._gioLam;
}
function capnhatNV() {
  var nv = layThongTinTuForm();
  console.log("Cập nhật: ", nv);
  var viTri = dsnv.findIndex(function (item) {
    return item._tknv == nv._tknv;
  });
  dsnv[viTri] = nv;
  renderDSNV(dsnv);
}
function resetForm() {
  document.getElementById("formQLNV").reset();
}
