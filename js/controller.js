function renderDSNV(dsnv) {
  var contentHTML = "";
  for (var i = dsnv.length - 1; i >= 0; i--) {
    var item = dsnv[i];
    var contentTr = `
    <tr>
      <td>${item._tknv}</td>
      <td>${item._name}</td>
      <td>${item._email}</td>
      <td>${item._datepicker}</td>
      <td>${item._chucVu}</td>
      <td>0</td>
      <td>0</td>
      <td>
        <button onclick="xoaNV('${item._tknv}')" class="btn btn-danger">Xóa</button>
        <button onclick="suaNV('${item._tknv}')" class="btn btn-warning" data-toggle="modal"
        data-target="#myModal">Sửa</button>
      </td>
    </tr>
    `;
    contentHTML += contentTr;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function layThongTinTuForm() {
  var tknv = document.getElementById("tknv").value;
  var name = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var password = document.getElementById("password").value;
  var datepicker = document.getElementById("datepicker").value;
  var luongCB = document.getElementById("luongCB").value;
  var chucVu = document.getElementById("chucVu").value;
  var gioLam = document.getElementById("gioLam").value;

  return {
    _tknv: tknv,
    _name: name,
    _email: email,
    _password: password,
    _datepicker: datepicker,
    _luongCB: luongCB,
    _chucVu: chucVu,
    _gioLam: gioLam,
  };
}
